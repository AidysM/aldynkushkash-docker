from django.shortcuts import render

from news.models import Post


def index(request):
    posts = Post.objects.all()[:5]
    context = {'posts': posts}
    return render(request, 'home/index.html', context)

