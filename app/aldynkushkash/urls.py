from django.contrib import admin
from django.contrib.flatpages import views
from django.urls import path, include


urlpatterns = [
    path('admin/', admin.site.urls),
    path('home/', include('home.urls', namespace='home')),
    path('news/', include('news.urls', namespace='news')),
    path('about/', views.flatpage, {'url': '/about/'}, name='about'),
    path('shop/', include('shop.urls', namespace='shop')),
    path('', include('protect.urls')),
    path('sign/', include('sign.urls')),
    path('accounts/', include('allauth.urls')),
]
