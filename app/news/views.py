from django.contrib.auth import models
from django.db.models import query
from django.shortcuts import render

from django.views.generic import ListView, DetailView, UpdateView, CreateView, DeleteView

from .models import Post


class PostListView(ListView):
    model = Post
    template_name = 'news/post_list.html'
    context_object_name = 'posts'
    queryset = Post.objects.order_by('-created')
    paginate_by = 10
    
    def get_context_data(self, **kwargs):
        return super().get_context_data(**kwargs)
    

class PostDetailView(DetailView):
    template_name = 'news/post_detail.html'
    queryset = Post.objects.all()
    


    