from django.urls import path

from .views import PostListView, PostDetailView # , NewCreateView, NewUpdateView, NewDeleteView


app_name = 'news'

urlpatterns = [
    path('', PostListView.as_view(), name='post_list'),
    path('<int:pk>/', PostDetailView.as_view(), name='post_detail'),
    # path('create/', NewCreateView.as_view(), name='new_create'),
    # path('<int:pk>/update/', NewUpdateView.as_view(), name='new_update'),
    # path('<int:pk>/delete/', NewDeleteView.as_view(), name='new_delete'),

]
