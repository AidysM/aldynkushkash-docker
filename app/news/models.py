from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

from .utils import get_timestamp_path


class Author(models.Model):
    author = models.CharField(max_length=200)
    #user = models.OneToOneRel(User, on_delete=models.CASCADE)
    
    def __str__(self) -> str:
        return f'{self.author}'


class Post(models.Model):
    STATUS_CHOICES = (
        ('draft', 'Draft'),
        ('published', 'Published'),
    )
    title = models.CharField(max_length=256, verbose_name='Заголовок')
    author = models.ForeignKey(Author, on_delete=models.CASCADE, verbose_name='Автор')
    image = models.ImageField(blank=True, upload_to=get_timestamp_path, verbose_name='Изображение')
    text = models.TextField(verbose_name='Содержание')
    publish = models.DateTimeField(default=timezone.now())
    created = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='draft')
    
    def __str__(self) -> str:
        return f'{self.title}'
    
    class Meta:
        ordering = ('-publish',)
        
    
